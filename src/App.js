import React, { useState } from "react";
import "./App.css";
import Todo from "./Todo";
import TodoForm from "./TodoForm";

const App = () => {
  const [todos, setTodos] = useState([
    { text: "Learn Hooks", isCompleted: true },
    { text: "Buy Products", isCompleted: false },
    { text: "Clean Home", isCompleted: true },
    { text: "Wash Car", isCompleted: false }
  ]);

  const addTodo = text => {
    const newTodos = [...todos, { text, isCompleted: false }];
    setTodos(newTodos);
  };

  const completeTodo = index => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = !newTodos[index].isCompleted;
    setTodos(newTodos);
  };

  const deleteTodo = index => {
    // const newTodos = [...todos];
    // newTodos.splice(1, index);
    setTodos(todos.filter((todo, id) => id !== index));
  };

  return (
    <div className="app">
      <div className="todo-list">
        <h1>Todo List:</h1>
        {todos.map((todo, index) => (
          <Todo
            todo={todo}
            index={index}
            key={index}
            completeTodo={completeTodo}
            deleteTodo={deleteTodo}
          />
        ))}
        <TodoForm addTodo={addTodo} />
      </div>
    </div>
  );
};

export default App;
